using UnityEngine;
using UnityEngine.Audio;

namespace Script.Week10
{
    [CreateAssetMenu(menuName = "gamedev3-week1/week10/SoundSettingPreset", fileName = "SoundSettingPreset")]
    public class SoundSetting : ScriptableObject
    {
        public AudioMixer Audiomixer;

        [Header("MasterVolumn")] public string MasterVolumnName = "MasterVolumn";
        [Range(-80, 20)] public float MasterVolumn;
        
        [Header("MasterVolumn")] public string MusicVolumnName = "MusicVolumn";
        [Range(-80, 20)] public float MusicVolumn;
        
        [Header("MasterVolumn")] public string MasterSFXVolumnName = "MasterSFXVolumn";
        [Range(-80, 20)] public float MasterSFXVolumn;
        
        [Header("MasterVolumn")] public string SFXVolumnName = "SFXVolumn";
        [Range(-80, 20)] public float SFXVolumn;
        
        [Header("MasterVolumn")] public string UIVolumnName = "UIVolumn";
        [Range(-80, 20)] public float UIVolumn;
    }
}
