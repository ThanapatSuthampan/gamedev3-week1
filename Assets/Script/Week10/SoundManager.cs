using System;
using UnityEngine;
using UnityEngine.UI;

namespace Script.Week10
{
    public class SoundManager : MonoBehaviour
    {
        [SerializeField] protected SoundSetting m_SoundSettings;

        public Slider m_SliderMasterVolumn;
        public Slider m_SliderMusicVolumn;
        public Slider m_SliderMasterSFXVolumn;
        public Slider m_SliderSFXVolumn;
        public Slider m_SliderUIVolumn;

        private void Start()
        {
            InitializeVolumns();
        }

        private void InitializeVolumns()
        {
            SetMasterVolumn(m_SoundSettings.MasterVolumn);
            SetMusicVolumn(m_SoundSettings.MusicVolumn);
            SetMasterSFXVolumn(m_SoundSettings.MasterSFXVolumn);
            SetSFXVolumn(m_SoundSettings.SFXVolumn);
            SetUIVolumn(m_SoundSettings.UIVolumn);
        }

        public void SetMasterVolumn(float vol)
        {
            //Set float to the audiomixer
            m_SoundSettings.Audiomixer.SetFloat(m_SoundSettings.MasterVolumnName, vol);
            
            //Set float to the scriptable object to persist the value although the game is closed
            m_SoundSettings.MasterVolumn = vol;
            
            //Set the slider bar's value
            m_SliderMasterVolumn.value = m_SoundSettings.MasterVolumn;
        }
        
        public void SetMusicVolumn(float vol)
        {
            //Set float to the audiomixer
            m_SoundSettings.Audiomixer.SetFloat(m_SoundSettings.MusicVolumnName, vol);
            
            //Set float to the scriptable object to persist the value although the game is closed
            m_SoundSettings.MusicVolumn = vol;
            
            //Set the slider bar's value
            m_SliderMusicVolumn.value = m_SoundSettings.MusicVolumn;
        }
        
        public void SetMasterSFXVolumn(float vol)
        {
            //Set float to the audiomixer
            m_SoundSettings.Audiomixer.SetFloat(m_SoundSettings.MasterSFXVolumnName, vol);
            
            //Set float to the scriptable object to persist the value although the game is closed
            m_SoundSettings.MasterSFXVolumn = vol;
            
            //Set the slider bar's value
            m_SliderMasterSFXVolumn.value = m_SoundSettings.MasterSFXVolumn;
        }
        
        public void SetSFXVolumn(float vol)
        {
            //Set float to the audiomixer
            m_SoundSettings.Audiomixer.SetFloat(m_SoundSettings.SFXVolumnName, vol);
            
            //Set float to the scriptable object to persist the value although the game is closed
            m_SoundSettings.SFXVolumn = vol;
            
            //Set the slider bar's value
            m_SliderSFXVolumn.value = m_SoundSettings.SFXVolumn;
        }
        
        public void SetUIVolumn(float vol)
        {
            //Set float to the audiomixer
            m_SoundSettings.Audiomixer.SetFloat(m_SoundSettings.UIVolumnName, vol);
            
            //Set float to the scriptable object to persist the value although the game is closed
            m_SoundSettings.UIVolumn = vol;
            
            //Set the slider bar's value
            m_SliderUIVolumn.value = m_SoundSettings.UIVolumn;
        }
    }
}