using UnityEngine;

namespace Suthampan_GameDev3_Week5
{
    public interface IActorEnterExitHandler
    {
        void ActorEnter(GameObject actor);
        void ActorExit(GameObject actor);
    }
}