using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Suthampan_GameDev3_Week5
{
    public abstract class PlayerControllerWithPreset : MonoBehaviour, IPlayerController
    {
        [SerializeField] protected CapsulePlayerControllerSettingPreset m_Preset;

        protected void Update()
        {
            Keyboard keyboard = Keyboard.current;

            if (keyboard[m_Preset.TurnleftKey].isPressed)
            {
                TurnLeft();
            }
            else if (keyboard[m_Preset.TurnrightKey].isPressed)
            {
                TurnRight();
            }

            if (keyboard[m_Preset.ForwardKey].isPressed)
            {
                if (keyboard[m_Preset.SprintKey].isPressed)
                {
                    MoveForwardSprint();
                }
                else
                {
                    MoveForward();
                }
            }
            else if (keyboard[m_Preset.backwardKey].isPressed)
            {
                MoveBackward();
            }
        }

        public abstract void MoveForward();
        public abstract void MoveForwardSprint();
        public abstract void MoveBackward();
        public abstract void TurnLeft();
        public abstract void TurnRight();
    }
}