using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Suthampan_GameDev3_Week5
{
    public class Week5CapsulePlayerControllerWithActorTrigger : Week5CapSulePlayerControllerWithPreset
    {
        [SerializeField] protected ActionTriggerHandler m_ActionTriggerHandler;

        protected void Update()
        {
            base.Update();
            
            Keyboard keyboard = Keyboard.current;

            if (keyboard[m_Preset.InteractionKey].wasPressedThisFrame)
            {
                PerformInteraction();
            }
        }

        protected virtual void PerformInteraction()
        {
            var interactable = m_ActionTriggerHandler.GetInteractable();

            if (interactable != null)
            {
                interactable.Interact(gameObject);
            }
        }
    }
}