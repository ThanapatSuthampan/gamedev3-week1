using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Suthampan_GameDev3_Week5
{
    public abstract class PlayerController : MonoBehaviour, IPlayerController
    {
        [SerializeField] protected float m_RotationSpeed = 180;
        [SerializeField] protected float m_directionalSpeed = 3;
        [SerializeField] protected float m_DirectionalSprintSpeed = 5;
        
        [Header("Key Config")]
        [SerializeField] protected Key m_ForwardKey = Key.W;
        [SerializeField] protected Key m_backwardKey = Key.S;
        [SerializeField] protected Key m_TurnLeftKey = Key.A;
        [SerializeField] protected Key m_TurnRightKey = Key.D;

        [SerializeField] protected Key m_SprintKey = Key.LeftShift;

        protected virtual void Update()
        {
            Keyboard keyboard = Keyboard.current;

            if (keyboard[m_TurnLeftKey].isPressed)
            {
                TurnLeft();
            }
            else if (keyboard[m_TurnRightKey].isPressed)
            {
                TurnRight();
            }

            /*float speedMagnitude = m_directionalSpeed;
            if (keyboard[Key.LeftShift].isPressed)
            {
                speedMagnitude = m_DirectionalSprintSpeed;
            }*/

            if (keyboard[m_ForwardKey].isPressed)
            {
                if (keyboard[m_SprintKey].isPressed)
                {
                    MoveForwardSprint();
                }
                else
                {
                    MoveForward();
                }
            }
            else if (keyboard[m_backwardKey].isPressed)
            {
                MoveBackward();
            }
        }

        public abstract void MoveForward();
        public abstract void MoveForwardSprint();
        public abstract void MoveBackward();
        public abstract void TurnLeft();
        public abstract void TurnRight();
    }
}