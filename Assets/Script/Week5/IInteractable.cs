using System.Collections;
using UnityEngine;

namespace Suthampan_GameDev3_Week5
{
    public interface IInteractable
    {
        void Interact(GameObject actor);
    }
}