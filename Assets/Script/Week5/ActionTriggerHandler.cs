using System;
using System.Collections.Generic;
using UnityEngine;

namespace Suthampan_GameDev3_Week5
{
    public class ActionTriggerHandler : MonoBehaviour
    {
        [SerializeField] protected List<GameObject> m_TriggeredGameObjects = new();
        [SerializeField] protected GameObject m_actor;

        protected virtual void Start()
        {
            if (m_actor == null)
            {
                m_actor = GetComponentInParent<Transform>().gameObject;
            }
        }

        protected virtual void OnTriggerEnter(Collider other)
        {
            var interactableComponants = other.GetComponents<IInteractable>();

            if (interactableComponants != null)
            {
                foreach (var ic in interactableComponants)
                {
                    if (ic is IActorEnterExitHandler enterExitHandler)
                    {
                        enterExitHandler.ActorEnter(m_actor);
                    }
                }
                
                m_TriggeredGameObjects.Add(other.gameObject);
            }
        }

        private void OnTriggerStay(Collider other)
        {
            
        }

        protected virtual void OnTriggerExit(Collider other)
        {
            var interactableComponants = other.GetComponents<IInteractable>();

            if (interactableComponants != null)
            {
                foreach (var ic in interactableComponants)
                {
                    if (ic is IActorEnterExitHandler enterExitHandler)
                    {
                        enterExitHandler.ActorExit(m_actor);
                    }
                }

                m_TriggeredGameObjects.Remove(other.gameObject);
            }
        }

        public virtual IInteractable GetInteractable()
        {
            //Remove null object from the list
            m_TriggeredGameObjects.RemoveAll(gameject => gameject == null);

            if (m_TriggeredGameObjects.Count == 0)
            {
                return null;
            }

            return m_TriggeredGameObjects[0].GetComponent<IInteractable>();
        }
    }
}