using System;
using System.Collections;
using TMPro;
using UnityEngine;

namespace Suthampan_GameDev3_Week5
{
    public class PokableObject : MonoBehaviour, IInteractable, IActorEnterExitHandler
    {
        [SerializeField] protected TextMeshProUGUI m_InteractionTxt;
        [SerializeField] protected float m_Power = 10;
        private Rigidbody m_rigidbody;
        private IInteractable m_InteractableImplementation;

        private void Start()
        {
            m_rigidbody = GetComponent<Rigidbody>();
        }

        public void Interact(GameObject actor)
        {
            m_rigidbody.AddForce(Vector3.up * m_Power, ForceMode.Impulse);
        }

        public void ActorEnter(GameObject actor)
        {
            m_InteractionTxt.gameObject.SetActive(true);
        }

        public void ActorExit(GameObject actor)
        {
            m_InteractionTxt.gameObject.SetActive(false);
        }
        
    }
}