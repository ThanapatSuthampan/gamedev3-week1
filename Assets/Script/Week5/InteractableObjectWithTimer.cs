using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Suthampan_GameDev3_Week5
{
    public class InteractableObjectWithTimer : MonoBehaviour, IInteractable, IActorEnterExitHandler
    {
        [SerializeField] protected TextMeshProUGUI m_TextInfoEToInteract;
        [SerializeField] protected float m_TimerDuration = 5;

        [SerializeField] protected Slider m_SlideTimer;
        private bool _IsTimerStart = false;
        private float _StartTimeStamp;
        private float _EndTimeStamp;
        private float _SliderValue;
        private IInteractable m_InteractableImplementation;

        public void ActorEnter(GameObject actor)
        {
            m_TextInfoEToInteract.gameObject.SetActive(true);
        }

        public void ActorExit(GameObject actor)
        {
            m_TextInfoEToInteract.gameObject.SetActive(false);
        }

        public void Interact(GameObject actor)
        {
            StartTimer();
        }

        private void StartTimer()
        {
            //Check if the timer is already running
            if (_IsTimerStart) return;

            _IsTimerStart = true;
            _StartTimeStamp = Time.time;
            _EndTimeStamp = Time.time + m_TimerDuration;
            _SliderValue = 0;
        }

        private void Update()
        {
            if (!_IsTimerStart) return;

            _SliderValue = ((Time.time - _StartTimeStamp) / m_TimerDuration) * m_SlideTimer.maxValue;
            m_SlideTimer.value = _SliderValue;

            if (Time.time >= _EndTimeStamp)
            {
                _IsTimerStart = false;
            }
        }
    }
}