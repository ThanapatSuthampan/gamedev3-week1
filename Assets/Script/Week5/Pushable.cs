using System;
using System.Collections;
using TMPro;
using UnityEngine;

namespace Suthampan_GameDev3_Week5
{
    [RequireComponent(typeof(Rigidbody))]
    public class Pushable : MonoBehaviour, IInteractable, IActorEnterExitHandler
    {
        [SerializeField] protected TextMeshProUGUI m_InteractionTxt;
        [SerializeField] protected float m_Power = 10;
        private Rigidbody m_rigidbody;
        private IInteractable m_InteractableImplementation;

        private void Start()
        {
            m_rigidbody = GetComponent<Rigidbody>();
        }

        public void Interact(GameObject actor)
        {
            m_rigidbody.AddForce(actor.transform.forward * m_Power, ForceMode.Impulse);
        }

        public void ActorEnter(GameObject actor)
        {
            m_InteractionTxt.gameObject.SetActive(true);
        }

        public void ActorExit(GameObject actor)
        {
            m_InteractionTxt.gameObject.SetActive(false);
        }
    }
}