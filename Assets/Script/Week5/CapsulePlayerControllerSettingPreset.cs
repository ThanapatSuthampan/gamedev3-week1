using UnityEngine;
using UnityEngine.InputSystem;

namespace Suthampan_GameDev3_Week5
{
    [CreateAssetMenu(fileName = "CapsulePlayerControllerSettingPreset", 
        menuName = "gamedev3-week1/Week5/CapsulePlayerControllerSettingPreset", 
        order = 0)]
    
    public class CapsulePlayerControllerSettingPreset : ScriptableObject
    {
        public float RotationSpeed = 180;
        public float DirectionalSpeed = 3;
        public float DirectionalSprintSpeed = 5;

        [Header("Keys Config")] 
        public Key ForwardKey = Key.W;
        public Key backwardKey = Key.S;

        public Key TurnleftKey = Key.A;
        public Key TurnrightKey = Key.D;

        public Key SprintKey = Key.LeftShift;

        public Key InteractionKey = Key.E;
    }
}