using UnityEngine;

namespace Suthampan_GameDev3_Week5
{
    public class Week5CapsulePlayerController : PlayerController
    {
        public override void MoveForward()
        {
            transform.Translate(transform.forward * m_directionalSpeed * Time.deltaTime, Space.World);
        }
        
        public override void MoveForwardSprint()
        {
            transform.Translate(transform.forward * m_DirectionalSprintSpeed * Time.deltaTime, Space.World);
        }
        
        public override void MoveBackward()
        {
            transform.Translate(-transform.forward * (m_directionalSpeed * 0.4f) * Time.deltaTime, Space.World);
        }
        
        public override void TurnLeft()
        {
            transform.Rotate(transform.up, -m_RotationSpeed  * Time.deltaTime, Space.Self);
        }
        
        public override void TurnRight()
        {
            transform.Rotate(transform.up, m_RotationSpeed  * Time.deltaTime, Space.Self);
        }
    }
}