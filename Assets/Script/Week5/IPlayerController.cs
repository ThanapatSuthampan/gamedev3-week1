namespace Suthampan_GameDev3_Week5
{
    public interface IPlayerController
    {
        void MoveForward();
        void MoveForwardSprint();

        void MoveBackward();

        void TurnLeft();
        void TurnRight();
    }
}