using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Suthampan_GameDev3_Week6
{
    public class GameAppFlowManager : MonoBehaviour
    {
        protected static bool IsScenenOptionLoaded;

        public void LoadScene(string sceneName)
        {
            SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
        }

        public void UnloadScene(string sceneName)
        {
            SceneManager.UnloadSceneAsync(sceneName);
        }

        public void LoadSceneAdditive (string sceneName)
        {
            SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);
        }

        public void LoadOptionScene (string optionSceneName)
        {
            if (!IsScenenOptionLoaded)
            {
                SceneManager.LoadScene(optionSceneName, LoadSceneMode.Additive);
                IsScenenOptionLoaded = true;
            }
        }

        public void UnloadOptionScene(string optionSceneName)
        {
            if (IsScenenOptionLoaded)
            {
                SceneManager.UnloadSceneAsync(optionSceneName);
                IsScenenOptionLoaded = false;
            }
        }

        public void ExitGame()
        {
            Application.Quit();
        }

        private void OnEnable()
        {
            SceneManager.sceneUnloaded += SceneUnloadEventHandler;
            SceneManager.sceneLoaded += SceneLoadEventHandler;
        }

        private void OnDisable()
        {
            SceneManager.sceneUnloaded -= SceneUnloadEventHandler;
            SceneManager.sceneLoaded -= SceneLoadEventHandler;
        }

        private void SceneUnloadEventHandler(Scene scene)
        {
            
        }

        private void SceneLoadEventHandler(Scene scene, LoadSceneMode mode)
        {
            if (scene.name.CompareTo("SceneOption") != 0)
            {
                IsScenenOptionLoaded = false;
            }
        }
    }
}