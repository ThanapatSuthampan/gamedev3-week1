using UnityEngine;

namespace Suthampan_GameDev3_Week6
{
    public class Push : MonoBehaviour
    {
        //[SerializeField] protected TextMeshProUGUI m_InteractionTxt;
        [SerializeField] protected float m_Power = 10;
        private Rigidbody m_rigidbody;
        //private IInteractable m_InteractableImplementation;

        private void Start()
        {
            m_rigidbody = GetComponent<Rigidbody>();
        }

        public void Interact(GameObject actor)
        {
            m_rigidbody.AddForce(actor.transform.forward * m_Power, ForceMode.Impulse);
        }

        public void ActorEnter(GameObject actor)
        {
            //m_InteractionTxt.gameObject.SetActive(true);
        }

        public void ActorExit(GameObject actor)
        {
            //m_InteractionTxt.gameObject.SetActive(false);
        }

        public void SelfDestroy(GameObject actor)
        {
            Destroy(actor);
        }
    }
}