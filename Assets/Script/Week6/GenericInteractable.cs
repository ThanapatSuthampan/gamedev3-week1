using Suthampan_GameDev3_Week5;
using UnityEngine;
using UnityEngine.Events;

namespace Suthampan_GameDev3_Week6
{
    public class GenericInteractable : MonoBehaviour, IInteractable, IActorEnterExitHandler
    {
        [SerializeField] protected UnityEvent<GameObject> m_OnPush_Interact = new();
        
        [SerializeField] protected UnityEvent<GameObject> m_OnPush_Enter = new();
        [SerializeField] protected UnityEvent<GameObject> m_OnPush_Exit = new();

        public virtual void Interact(GameObject actor)
        {
            //m_OnInteract.Invoke();
            m_OnPush_Interact.Invoke(actor);
        }

        public virtual void ActorEnter(GameObject actor)
        {
            //m_OnactorEnter.Invoke();
            m_OnPush_Enter.Invoke(actor);
        }

        public virtual void ActorExit(GameObject actor)
        {
            //m_OnActorExit.Invoke();
            m_OnPush_Exit.Invoke(actor);
        }
    }
}