using System;
using UnityEngine;
using UnityEngine.Events;

namespace Suthampan_GameDev3_Week6
{
    public class Timer : MonoBehaviour
    {
        [SerializeField] protected float m_TimerDuration = 5;

        [SerializeField] protected UnityEvent m_TimerStartEvent = new();
        [SerializeField] protected UnityEvent m_timerEndEvent = new();

        protected bool _IstimerStart = false;
        protected float _StartTimeStamp;
        protected float _EndTimeStamp;

        [SerializeField] protected float _CurrentTime;

        private void Update()
        {
            if (!_IstimerStart) return;

            _CurrentTime = (Time.time - _StartTimeStamp);
            
            if(Time.time >= _EndTimeStamp) EndTimer();
        }

        public virtual void StartTimer()
        {
            if (_IstimerStart) return;
            
            m_TimerStartEvent.Invoke();
            _IstimerStart = true;
            _StartTimeStamp = Time.time;
            _EndTimeStamp = Time.time + m_TimerDuration;
            Debug.Log("Time is counting");
        }

        public virtual void EndTimer()
        {
            m_timerEndEvent.Invoke();
            _IstimerStart = false;
        }
    }
}