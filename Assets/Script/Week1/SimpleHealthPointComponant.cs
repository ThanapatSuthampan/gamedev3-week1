using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

namespace Suthampan.GameDev3.Chapter2.GameDev3.Chapter1
{
    public class SimpleHealthPointComponant : MonoBehaviour
    {
        [SerializeField] public const float Max_HP = 100;

        [SerializeField] private float m_HealthPoint;

        //Property
        public float HealthPoint
        {
            get { return m_HealthPoint; }
            set
            {
                m_HealthPoint = value;
                /*if (value > 0)
                {
                    if (value <= Max_HP)
                    {
                        m_HealthPoint = value;
                    }
                    else
                    {
                        m_HealthPoint = Max_HP;
                    }
                }*/
            }
        }
    }
}


