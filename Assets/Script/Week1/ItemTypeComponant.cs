using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Suthampan.GameDev3.Chapter2.GameDev3.Chapter1
{
    public class ItemTypeComponant : MonoBehaviour
    {
        [SerializeField] protected ItemType m_ItemType;

        public ItemType Type
        {
            get { return m_ItemType;}
            set { m_ItemType = value;}
        }
    }
}

