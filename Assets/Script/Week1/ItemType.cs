using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Suthampan.GameDev3.Chapter2.GameDev3.Chapter1
{
    public enum ItemType
    {
        Coin,
        Bigcoin,
        Powerup,
        Powerdown
    }
}
