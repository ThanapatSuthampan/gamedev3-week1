using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace Suthampan.GameDev3.Chapter2.GameDev3.Chapter1
{
    public class ScriptLifeCycleStudy : MonoBehaviour
    {
        private int m_CallingSequence = 1;


        private void Awake()
        {
            Debug.LogFormat("{0} {1} has been called.", m_CallingSequence++, MethodBase.GetCurrentMethod().Name);
        }

        // Start is called before the first frame update
        void Start()
        {
            Debug.LogFormat("{0} {1} has been called.", m_CallingSequence++, MethodBase.GetCurrentMethod().Name);
        }

        // Update is called once per frame
        void Update()
        {
        
        }

        private void FixedUpdate()
        {
            
        }

        private void OnEnable()
        {
            Debug.LogFormat("{0} {1} has been called.", m_CallingSequence++, MethodBase.GetCurrentMethod().Name);
        }

        private void OnDisable()
        {
            Debug.LogFormat("{0} {1} has been called.", m_CallingSequence++, MethodBase.GetCurrentMethod().Name);
        }

        private void OnDestroy()
        {
            Debug.LogFormat("{0} {1} has been called.", m_CallingSequence++, MethodBase.GetCurrentMethod().Name);
        }

        private void OnApplicationPause(bool pauseStatus)
        {
            Debug.LogFormat("{0} {1} has been called.", m_CallingSequence++, MethodBase.GetCurrentMethod().Name);
        }

        private void OnApplicationQuit()
        {
            Debug.LogFormat("{0} {1} has been called.", m_CallingSequence++, MethodBase.GetCurrentMethod().Name);
        }
    }
}

