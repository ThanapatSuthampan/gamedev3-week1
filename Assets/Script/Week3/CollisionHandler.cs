using System;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace Suthampan_GameDev3_Chapter3
{
    public class CollisionHandler : MonoBehaviour
    {
        [SerializeField] protected List<GameObject> m_TriggerGameObject = new();
        [SerializeField] protected List<GameObject> m_CollidedGameObject = new();

        private void OnTriggerEnter(Collider other)
        {
            m_TriggerGameObject.Add(other.gameObject);
        }

        private void OnTriggerStay(Collider other)
        {
            
        }

        private void OnTriggerExit(Collider other)
        {
            m_TriggerGameObject.Remove(other.gameObject);
        }

        private void OnCollisionEnter(Collision other)
        {
            m_CollidedGameObject.Add(other.gameObject);
        }

        private void OnCollisionStay(Collision other)
        {
            
        }

        private void OnCollisionExit(Collision other)
        {
            m_CollidedGameObject.Remove(other.gameObject);
        }
    }
}