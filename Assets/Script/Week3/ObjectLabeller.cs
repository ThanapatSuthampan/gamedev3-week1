using System;
using System.Numerics;
using UnityEngine;
using UnityEngine.UI;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

namespace Suthampan_GameDev3_Chapter3
{
    public class ObjectLabeller : MonoBehaviour
    {
        [SerializeField] protected string m_Label = "";
        [SerializeField] protected Color m_Colour = Color.black;
        [SerializeField] protected Vector3 m_TextOffset = Vector3.zero;

        private Canvas m_Canvas;
        private RectTransform m_RectTransform;
        private Text m_TextLable;

        public string Label
        {
            get { return m_TextLable.text; }
            set { m_Label = value;
                m_TextLable.text = m_Label;
            }
        }

        private void Awake()
        {
            var canvasGo = new GameObject("Canvas");
            canvasGo.transform.SetParent(transform);
            m_Canvas = canvasGo.AddComponent<Canvas>();
            m_Canvas.renderMode = RenderMode.WorldSpace;

            canvasGo.AddComponent<CanvasScaler>();
            m_RectTransform = canvasGo.GetComponent<RectTransform>();
            m_RectTransform.localPosition = Vector3.zero;

            var textGo = new GameObject("TextLabel");
            textGo.transform.SetParent(canvasGo.transform);
            m_TextLable = textGo.AddComponent<Text>();
            m_TextLable.transform.localPosition = Vector3.up + m_TextOffset;
            m_TextLable.transform.localScale = new Vector3(0.02f, 0.02f, 0.02f);
            m_TextLable.alignment = TextAnchor.MiddleCenter;
            m_TextLable.text = m_Label == "" ? gameObject.name : m_Label;
            m_TextLable.font = Font.CreateDynamicFontFromOSFont("Arial", 16);
            m_TextLable.color = m_Colour;

            m_Label = m_TextLable.text;
        }

        private void Start()
        {
            
        }

        private void Update()
        {
            m_Canvas.transform.rotation =
                Quaternion.LookRotation(m_Canvas.transform.position - Camera.main.transform.position); ;
        }
    }
}