using Suthampan_GameDev3_Week5;
using Suthampan.GameDev3.Chapter2.GameDev3.Chapter1;
using UnityEngine;

namespace Script.Week9
{
    [RequireComponent(typeof(ItemTypeComponant))]
    public class Pickupable : MonoBehaviour, IInteractable, IActorEnterExitHandler
    {
        public void Interact(GameObject actor)
        {
            //myself
            var itemTypeComponent = GetComponent<ItemTypeComponant>();
            //actor
            var inventory = actor.GetComponent<IInventory>();
            inventory.AddItem(itemTypeComponent.Type.ToString(), 1);
            
            Destroy(gameObject);
        }

        public void ActorEnter(GameObject actor)
        {
            
        }

        public void ActorExit(GameObject actor)
        {
            
        }
    }
}