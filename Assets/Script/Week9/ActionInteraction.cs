using System;
using System.Linq;
using Suthampan_GameDev3_Week5;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace Script.Week9
{
    public class ActionInteraction : MonoBehaviour
    {
        [SerializeField] protected ActorTriggerHandlerV2 m_ActorTriggerHandler;

        [SerializeField] protected Key m_InteractionKey;

        [SerializeField] protected UnityEvent m_OnStartInteract = new();

        private void Start()
        {
            if (m_ActorTriggerHandler == null)
            {
                m_ActorTriggerHandler = GetComponentInChildren<ActorTriggerHandlerV2>();
            }
        }

        private void Update()
        {
            Keyboard keyboard = Keyboard.current;

            if (keyboard[m_InteractionKey].wasPressedThisFrame)
            {
                PerformInteraction();
            }
        }

        protected virtual void PerformInteraction()
        {
            var interactables = m_ActorTriggerHandler.GetInteractables();

            if (interactables?.Length > 0)
            {
                m_OnStartInteract.Invoke();

                foreach (var interactable in interactables)
                {
                    interactable.Interact(gameObject);
                }
            }
        }
    }
}