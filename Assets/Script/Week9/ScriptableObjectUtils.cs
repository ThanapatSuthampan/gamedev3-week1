using UnityEngine;

namespace Script.Week9
{
    [CreateAssetMenu(menuName = "gamedev3-week1/Util/ScriptableObjectUtils")]
    public class ScriptableObjectUtils : ScriptableObject
    {
        public void Destroy(GameObject gameObject)
        {
            Object.Destroy(gameObject);
        }
    }
}