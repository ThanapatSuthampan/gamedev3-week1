using Suthampan_GameDev3_Week5;
using UnityEngine;

namespace Script.Week9
{
    public class ActorTriggerHandlerV2 : ActionTriggerHandler
    {
        public virtual IInteractable[] GetInteractables()
        {
            //Remove null object from the list
            m_TriggeredGameObjects.RemoveAll(gameject => gameject == null);

            if (m_TriggeredGameObjects.Count == 0)
            {
                return null;
            }

            return m_TriggeredGameObjects[0].GetComponents<IInteractable>();
        }
    }
}