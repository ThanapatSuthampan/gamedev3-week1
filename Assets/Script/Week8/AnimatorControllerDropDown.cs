using System;
using UnityEngine;
using UnityEngine.TextCore.Text;
using UnityEngine.UI;

namespace Script.Week8
{
    public class AnimatorControllerDropDown : MonoBehaviour
    {
        private Slider TurnSlider;

        [SerializeField] private GameObject Character;

        private void Start()
        {
            TurnSlider = GetComponent<Slider>();
        }

        public void TurnAnimation()
        {
            if (Character != null)
            {
                Animator animator = Character.GetComponent<Animator>();
                if (animator)
                {
                    animator.SetFloat("Turn", TurnSlider.value);
                }
            }
        }
        
        public void ForwardAnimation()
        {
            if (Character != null)
            {
                Animator animator = Character.GetComponent<Animator>();
                if (animator)
                {
                    animator.SetFloat("Forward", TurnSlider.value);
                }
            }
        }
    }
}