using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Suthampan.GameDev3.Chapter2.GameDev3.Chapter2
{
    public class ControlObjectUsingMouse : MonoBehaviour
    {
        private Vector3 mMousePreviousPosition;
        public float mMouseDeltaVectorScale = 0.5f;

        private void Start()
        {
            
        }

        private void Update()
        {
            Vector3 mouseCurrentPos = Input.mousePosition;
            Vector3 mouseDeltaVector = Vector3.zero;
            mouseDeltaVector = (mouseCurrentPos = mMousePreviousPosition).normalized;

            if (Input.GetMouseButton(0))
            {
                this.transform.Translate(mouseDeltaVector * mMouseDeltaVectorScale, Space.World);
            }
            
            if (Input.GetMouseButtonUp(0))
            {
                Debug.Log("Left Button Up");
            }
            else if (Input.GetMouseButtonDown(0))
            {
                Debug.Log("Left Button Down");
            }
            
            if (Input.GetMouseButtonUp(2))
            {
                Debug.Log("Right Button Up");
            }
            else if (Input.GetMouseButtonDown(2))
            {
                Debug.Log("Right Button Down");
            }
            
            if (Input.GetMouseButtonUp(2))
            {
                Debug.Log("Mid Button Up");
            }
            else if (Input.GetMouseButtonDown(2))
            {
                Debug.Log("Mid Button Down");
            }

            this.transform.Translate(0, 0, Input.mouseScrollDelta.y * mMouseDeltaVectorScale, Space.World);

            mMousePreviousPosition = mouseCurrentPos;
        }
    }
}
