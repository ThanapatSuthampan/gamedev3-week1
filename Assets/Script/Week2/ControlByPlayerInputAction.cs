using System;
using System.Collections;
using System.Collections.Generic;
using Suthampan.GameDev3.Chapter2.GameDev3.Chapter1;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Suthampan.GameDev3.Chapter2.GameDev3.Chapter2
{
    public class ControlByPlayerInputAction : MonoBehaviour
    {
        public float mMovementScale = 0.5f;

        public float mCurrentHorizontal;
        public float mCurrentVertical;

        protected bool m_IsFire1Held = false;
        protected bool m_IsFire2Held = false;

        public void OnHorizontal(InputValue value)
        {
            mCurrentHorizontal = value.Get<float>();
        }

        public void OnVertical(InputValue value)
        {
            mCurrentVertical = value.Get<float>();
        }

        public void OnFire1(InputValue value)
        {
            Debug.Log("Fire1");
            var v = value.Get<float>();

            if (value.isPressed)
            {
                ShootABulletInForwardDirection(Vector3.zero);

                if (v > UnityEngine.InputSystem.InputSystem.settings.defaultHoldTime)
                {
                    m_IsFire1Held = true;
                }
            }
            
            else
            {
                ShootABulletInForwardDirection(new Vector3(0,1,0), PrimitiveType.Capsule, 1.5f);
                m_IsFire1Held = false;
            }
        }

        public void OnFire2(InputValue value)
        {
            Debug.Log("Fire2");
            var v = value.Get<float>();

            if (value.isPressed)
            {
                var spinMovement = gameObject.AddComponent<SpinMovement>();

                if (v > UnityEngine.InputSystem.InputSystem.settings.defaultHoldTime)
                {
                    m_IsFire2Held = true;
                }
            }
            else
            {
                var spinMovement = gameObject.GetComponent<SpinMovement>();
                Destroy(spinMovement);
            }
        }

        private void Update()
        {
            float hMovement = mCurrentHorizontal * mMovementScale;
            float vMovement = mCurrentVertical * mMovementScale;
            
            transform.Translate(new Vector3(hMovement, vMovement, 0));

            if (m_IsFire1Held)
            {
                Debug.Log("Fire1-Hold");
            }

            if (m_IsFire2Held)
            {
                Debug.Log("Fire2-Hold");
            }
        }
        
        private void ShootABulletInForwardDirection(Vector3 forceModifier, PrimitiveType type = PrimitiveType.Sphere, float forceMagnitude = 1)
        {
            //Create a new primitive at runtime
            var newGameObject = GameObject.CreatePrimitive(type);
            
            //Start the newGameObject position w/ forward*1.5f
            newGameObject.transform.position = transform.position + transform.forward * 1.5f;
            
            //Add rigidbody to the newly created gameobject
            var rigidbody = newGameObject.AddComponent<Rigidbody>();
            rigidbody.mass = 0.15f;
            
            //calculate the shooting direction
            Vector3 shootingDirection = (forceModifier + transform.forward) * forceMagnitude;
            rigidbody.AddForce(shootingDirection, ForceMode.Impulse);
            
            //Destroy the new gameobject in 3 sec
            Destroy(newGameObject, 3);
        }
    }
}