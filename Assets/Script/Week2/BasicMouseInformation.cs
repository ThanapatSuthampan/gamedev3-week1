using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Suthampan.GameDev3.Chapter2.GameDev3.Chapter2
{
    public class BasicMouseInformation : MonoBehaviour
    {
        public Text mTextMousePosition;
        public Text mTextMouseScrollDelta;
        public Text mTextMouseDeltaVector;
        
        private Vector3 _mMousePreviousPosition;

        private void Update()
        {
            Vector3 mouseCurrentPos = Input.mousePosition;
            Vector3 mouseDeltaVector = Vector3.zero;
            mouseDeltaVector = (mouseCurrentPos - _mMousePreviousPosition).normalized;
   
            mTextMousePosition.text = Input.mousePosition.ToString();
            mTextMouseScrollDelta.text = Input.mouseScrollDelta.ToString();
            mTextMouseDeltaVector.text = mouseDeltaVector.ToString();

            _mMousePreviousPosition = mouseCurrentPos;
        }
    }
}