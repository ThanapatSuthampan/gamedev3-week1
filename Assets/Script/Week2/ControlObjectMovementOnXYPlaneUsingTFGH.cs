using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Suthampan.GameDev3.Chapter2.GameDev3.Chapter2
{
    public class ControlObjectMovementOnXYPlaneUsingTFGH : MonoBehaviour
    {
        public float mMovementStep;

        private void Start()
        {
            
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                this.transform.Translate(-mMovementStep, 0, 0);
            }
            else if (Input.GetKeyDown(KeyCode.H))
            {
                this.transform.Translate(mMovementStep, 0, 0);
            }
            else if (Input.GetKeyDown(KeyCode.T))
            {
                this.transform.Translate(0, mMovementStep, 0);
            }
            else if (Input.GetKeyDown(KeyCode.G))
            {
                this.transform.Translate(0, -mMovementStep, 0);
            }
            
            if (Input.GetKeyUp(KeyCode.F))
            {
                this.transform.Translate(-mMovementStep*(-1), 0, 0);
            }
            else if (Input.GetKeyUp(KeyCode.H))
            {
                this.transform.Translate(mMovementStep*(-1), 0, 0);
            }
            else if (Input.GetKeyUp(KeyCode.T))
            {
                this.transform.Translate(0, mMovementStep*(-1), 0);
            }
            else if (Input.GetKeyUp(KeyCode.G))
            {
                this.transform.Translate(0, -mMovementStep*(-1), 0);
            }
        }
    }
}