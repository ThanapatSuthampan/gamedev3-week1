using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Suthampan.GameDev3.Chapter2
{
    public class ControlObjectMovementOnXYPlaneUsingArrowKeys : MonoBehaviour
    {
        public float mMovementStep;
        
        // Start is called before the first frame update
        void Start()
        {
            
        }
    
        // Update is called once per frame
        void Update()
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                this.transform.Translate(-mMovementStep, 0, 0);
            }
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                this.transform.Translate(mMovementStep, 0, 0);
            }
            else if (Input.GetKey(KeyCode.UpArrow))
            {
                this.transform.Translate(0, mMovementStep, 0);
            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                this.transform.Translate(0, -mMovementStep, 0);
            }
        }
    }
}