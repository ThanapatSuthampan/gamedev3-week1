using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Suthampan.GameDev3.Chapter2.GameDev3.Chapter2
{
    public class ControlObjectMovementOnXYPlaneUsingWASD : MonoBehaviour
    {
        public float mMovementStep;

        private void Start()
        {
            
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                this.transform.Translate(-mMovementStep, 0, 0);
            }
            else if (Input.GetKeyDown(KeyCode.D))
            {
                this.transform.Translate(mMovementStep, 0, 0);
            }
            else if (Input.GetKeyDown(KeyCode.W))
            {
                this.transform.Translate(0, mMovementStep, 0);
            }
            else if (Input.GetKeyDown(KeyCode.S))
            {
                this.transform.Translate(0, -mMovementStep, 0);
            }
        }
    }
}