using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Suthampan.GameDev3.Chapter2.GameDev3.Chapter2
{
    public class InputSystemControlObjectMovementOnXYPlaneUsingArrowKeys : MonoBehaviour
    {
        public float mMovementStep;
        
        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
            Keyboard keyboard = Keyboard.current;

            if (keyboard[Key.LeftArrow].isPressed)
            {
                this.transform.Translate(-mMovementStep,0,0);
            }
            else if (keyboard[Key.RightArrow].isPressed)
            {
                this.transform.Translate(mMovementStep,0,0);
            }
            else if (keyboard[Key.UpArrow].isPressed)
            {
                this.transform.Translate(0,mMovementStep,0);
            }
            else if (keyboard[Key.DownArrow].isPressed)
            {
                this.transform.Translate(0,-mMovementStep,0);
            }
        }
    }
}
