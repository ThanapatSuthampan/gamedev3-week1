using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Script.Week11
{
    [RequireComponent(typeof (NavMeshAIController))]
    public class NavMeshAIDestinationSetter : MonoBehaviour
    {
        [SerializeField] protected NavMeshAIController m_NMAI;
        [SerializeField] private float surfaceOffset = 0;

        private void Start()
        {
            m_NMAI = GetComponent<NavMeshAIController>();
        }

        private void Update()
        {
            Mouse mouse = Mouse.current;

            if (mouse.leftButton.wasPressedThisFrame)
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hit) == true)
                {
                    GameObject pickedTarget = new GameObject();
                    pickedTarget.transform.position = hit.point + hit.normal * surfaceOffset;
                    
                    m_NMAI.SetTarget(pickedTarget.transform);
                    
                    Destroy(pickedTarget, 0.2f);
                }
            }
        }
    }
}