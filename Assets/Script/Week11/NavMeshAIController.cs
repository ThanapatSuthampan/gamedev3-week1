using System;
using StarterAssets;
using UnityEngine;

namespace Script.Week11
{
    [RequireComponent(typeof (UnityEngine.AI.NavMeshAgent))]
    [RequireComponent(typeof (ThirdPersonControllerAI))]
    public class NavMeshAIController : MonoBehaviour
    {
        [SerializeField] protected UnityEngine.AI.NavMeshAgent m_NavMeshAgent;
        [SerializeField] protected Transform m_Target;
        [SerializeField] protected ThirdPersonControllerAI m_AI;

        private void Start()
        {
            m_NavMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
            m_NavMeshAgent.updateRotation = true;
            m_NavMeshAgent.updatePosition = true;
            m_AI = GetComponent<ThirdPersonControllerAI>();
        }

        private void Update()
        {
            if (m_Target != null)
            {
                m_NavMeshAgent.SetDestination(m_Target.position);
            }

            if (m_NavMeshAgent.remainingDistance > m_NavMeshAgent.stoppingDistance)
            {
                if (m_Target != null)
                {
                    m_AI.Move(m_Target.position);
                }
            }
            else
            {
                if (m_Target != null)
                {
                    m_AI.Move(Vector3.zero, 0);
                    m_Target = null;
                }
            }
        }

        public void SetTarget(Transform target)
        {
            m_Target = target;
        }

        public Vector3 GetTransformPosition()
        {
            return m_Target.position;
        }
    } 
}