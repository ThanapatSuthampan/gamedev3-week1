using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Suthampan.GameDev3.Chapter2.GameDev3.Chapter1;
using UnityEditor;
using UnityEngine;

namespace Suthampan.GameDev3.Week4
{
    public class PlayerTriggerWithITC : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            //Get componants form item object
            //Get the ItemTypeComponant componants from the triggered object
            ItemTypeComponant itc = other.GetComponent<ItemTypeComponant>();
            
            //Get componants from the player
            //Inventory
            var inventory = GetComponent<Inventory>();
            
            //SimpleHealthPointComponant
            var simpleHP = GetComponent<SimpleHealthPointComponant>();

            if (itc != null)
            {
                switch (itc.Type)
                {
                    case ItemType.Coin:
                        inventory.AddItem("Coin", 1);
                        break;
                    case ItemType.Bigcoin:
                        inventory.AddItem("BigCoin", 1);
                        break;
                    case ItemType.Powerup:
                        if (simpleHP != null)
                            simpleHP.HealthPoint = simpleHP.HealthPoint + 10;
                        break;
                    case ItemType.Powerdown:
                        if (simpleHP != null)
                            simpleHP.HealthPoint = simpleHP.HealthPoint - 10;
                        break;
                }
            }
            
            Destroy(other.gameObject, 0);
        }
    }
}